/**
 * list.cpp
 * C++ implementation of question 3.9
 */

#include <iostream>

using namespace std;

int main(void)
{
	int criticalValue;
	
	int numCount;
	int greaterCount = 0;
	int lessCount = 0;
	
	cout << ":";
	cin >> criticalValue;
	cout << ":";
	cin >> numCount;
	
	while(numCount < 0)
	{
		cout << "0" << endl << ":";
		cin >> numCount;
	} // while()
	
	int numCounter;
	int input;
	
	for(numCounter = 0; numCounter < numCount; numCounter++)
	{
		cout << ":";
		cin >> input;
		
		if (input >= criticalValue)
			greaterCount++;
		else
			lessCount++;
	}
	
	cout << greaterCount << endl << lessCount << endl;
	
} // main()