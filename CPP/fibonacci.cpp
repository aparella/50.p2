/**
 * fibonacci.cpp
 * C++ implementation of question 3.10
 */
#include <iostream>

using namespace std;

int main(void)
{
	unsigned n;
	unsigned index;
	
	unsigned valueOne, valueTwo, temp;
	
	valueOne = valueTwo = 1;
	
	cout << ":";
	cin >> n;
	
	for (index = 2; index < n; index++)
	{
		temp = valueTwo;
		valueTwo = valueOne + valueTwo;
		valueOne = temp;
	} // for()
	
	cout << valueTwo << endl << endl;
	
	return 0;	
} // main()